# Coding Challenge

## Setting up

**Requirements**

* **Java 1.8**
* **Maven**
* **API client like Postman** OR **curl**
    
**Frameworks/Tools used:**

* **Spring Boot**
* **Spring HATEOAS**
* **Spring Data MongDB**
* **Hibernate Validator**
* **Apache Commons Validator**
* **Swagger 2**

**Fire up the application**

1. Resolve maven dependencies    
2. Start Spring Boot Application using any of [these options](https://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-running-your-application.html)
  
## How to use
**In-app API Documentation available at the following link**

 ```
 http://localhost:8080/api/docs
 ```
    
# Client Endpoint
    http://localhost:8080/api/clients
    
 * `curl` commands available for the examples below
 * But preferable to use Postman
    
**List Clients**
---
**Method : GET**

Note: Application is bootstrapped with few samples of data on startup 

    $ curl http://localhost:8080/api/clients

  
**Expected Response - 200 OK**
```
[ {
  "ipv4" : "192.168.1.1",
  "name" : "Richie Panganiban",
  "app" : "app1",
  "environment" : "PROD",
  "_links" : {
    "self" : {
      "href" : "http://localhost:8080/api/clients/5af5f600b8dd5d0d22f95c88"
    },
    "list" : {
      "href" : "http://localhost:8080/api/clients"
    },
    "delete" : {
      "href" : "http://localhost:8080/api/clients/5af5f600b8dd5d0d22f95c88"
    },
    "filter" : {
      "href" : "http://localhost:8080/api/clients/filter?query="
    }
  }
}, {
  "ipv4" : "127.0.0.1",
  "name" : "localhost",
  "app" : "app1",
  "environment" : "PROD",
  "_links" : {
    "self" : {
      "href" : "http://localhost:8080/api/clients/5af5f600b8dd5d0d22f95c89"
    },
    "list" : {
      "href" : "http://localhost:8080/api/clients"
    },
    "delete" : {
      "href" : "http://localhost:8080/api/clients/5af5f600b8dd5d0d22f95c89"
    },
    "filter" : {
      "href" : "http://localhost:8080/api/clients/filter?query="
    }
  }
}, {.....
..}]
``` 
**Get Client**
---
**Method : GET**

* Request path `{id}` is provided in `self` rel link from the listing. In the example below, we `GET` to resource `http://localhost:8080/api/clients/5af5f600b8dd5d0d22f95c88`

      $ curl http://localhost:8080/api/clients/{id}

**Expected Response - 200 OK**
```
{
  "ipv4" : "192.168.1.1",
  "name" : "Richie Panganiban",
  "app" : "app1",
  "environment" : "PROD",
  "_links" : {
    "self" : {
      "href" : "http://localhost:8080/api/clients/5af5f600b8dd5d0d22f95c88"
    },
    "list" : {
      "href" : "http://localhost:8080/api/clients"
    },
    "delete" : {
      "href" : "http://localhost:8080/api/clients/5af5f600b8dd5d0d22f95c88"
    },
    "filter" : {
      "href" : "http://localhost:8080/api/clients/filter?query="
    }
  }
}
``` 
**Add Client**
---

**Method : POST**

```
curl -H "Content-Type: application/json" -X POST -d '{"ipv4":"255.255.255.255","name":"Richardson","app":"app1","environment":"PROD"}' http://localhost:8080/api/clients/
```
**Expected Response - 200 OK**

```
{
  "ipv4" : "255.255.255.255",
  "name" : "Richardson",
  "app" : "app1",
  "environment" : "PROD",
  "_links" : {
    "self" : {
      "href" : "http://localhost:8080/api/clients/5af66a03d8e452037846c720"
    },
    "list" : {
      "href" : "http://localhost:8080/api/clients"
    },
    "delete" : {
      "href" : "http://localhost:8080/api/clients/5af66a03d8e452037846c720"
    },
    "filter" : {
      "href" : "http://localhost:8080/api/clients/filter?query="
    }
  }
```   

**Filter Client by [ipv4 | name | app | environment]**
---
**Method : GET** 

    $ curl http://localhost:8080/api/clients/filter?query={param}
    
* **Request parameter `{param}` is any string that finds a regex match from these fields:**

    * ipv4
    * name
    * app
    * environment

* **In this example, we query will query filter to find client names starting with "Rich"**
       
      curl http://localhost:8080/api/clients/filter?query=Rich    
  
**Expected Response - 200 OK**

```
[ {
  [ {
    "ipv4" : "192.168.1.1",
    "name" : "Richie Panganiban",
    "app" : "app1",
    "environment" : "PROD",
    "_links" : {
      "self" : {
        "href" : "http://localhost:8080/api/clients/5af66336d8e4520363abfebc"
      },
      "list" : {
        "href" : "http://localhost:8080/api/clients"
      },
      "delete" : {
        "href" : "http://localhost:8080/api/clients/5af66336d8e4520363abfebc"
      },
      "filter" : {
        "href" : "http://localhost:8080/api/clients/filter?query="
      }
    }
  }, {
    "ipv4" : "255.255.255.250",
    "name" : "Richardson",
    "app" : "app1",
    "environment" : "PROD",
    "_links" : {
      "self" : {
        "href" : "http://localhost:8080/api/clients/5af66345d8e4520363abfebf"
      },
      "list" : {
        "href" : "http://localhost:8080/api/clients"
      },
      "delete" : {
        "href" : "http://localhost:8080/api/clients/5af66345d8e4520363abfebf"
      },
      "filter" : {
        "href" : "http://localhost:8080/api/clients/filter?query="
      }
    }
} ]
```
**Delete Client**
---

**Method : DELETE**

    $ curl -X DELETE http://localhost:8080/api/clients/delete/{id} 

* Request path `{id}` is provided in `delete` rel link from the listing. In the example below, we `DELETE` resource `http://localhost:8080/api/clients/5af5f600b8dd5d0d22f95c88`

      $ curl -X http://localhost:8080/api/clients/delete/5af5f600b8dd5d0d22f95c88

**Expected Response - 200 OK**

```
{}
```  
 
 
