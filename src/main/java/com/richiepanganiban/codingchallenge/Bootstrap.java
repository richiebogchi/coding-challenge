package com.richiepanganiban.codingchallenge;

import com.richiepanganiban.codingchallenge.domain.Client;
import com.richiepanganiban.codingchallenge.repository.ClientRepository;
import com.richiepanganiban.codingchallenge.rest.hateoas.ClientResourceAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

@Component
public class Bootstrap implements ApplicationRunner {


    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    ClientResourceAssembler clientResourceAssembler;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        /* App1 - PROD users */
        Client client1 = new Client();
        client1.setIpv4("192.168.1.1");
        client1.setName("Richie Panganiban");
        client1.setApp("app1");
        client1.setEnvironment("PROD");

        Client client2 = new Client();
        client2.setIpv4("127.0.0.1");
        client2.setName("localhost");
        client2.setApp("app1");
        client2.setEnvironment("PROD");

        Client client3 = new Client();
        client3.setIpv4("8.8.8.8");
        client3.setName("Google");
        client3.setApp("app1");
        client3.setEnvironment("PROD");

        mongoTemplate.save(client1);
        mongoTemplate.save(client2);
        mongoTemplate.save(client3);
    }
}
