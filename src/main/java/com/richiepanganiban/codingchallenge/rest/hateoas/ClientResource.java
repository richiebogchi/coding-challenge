package com.richiepanganiban.codingchallenge.rest.hateoas;

import org.springframework.hateoas.ResourceSupport;

public class ClientResource extends ResourceSupport {

    private String ipv4;
    private String name;
    private String app;
    private String environment;

    public String getIpv4() {
        return ipv4;
    }

    public void setIpv4(String ipv4) {
        this.ipv4 = ipv4;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }
}
