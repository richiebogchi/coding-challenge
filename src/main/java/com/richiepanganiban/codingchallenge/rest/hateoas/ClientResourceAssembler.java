package com.richiepanganiban.codingchallenge.rest.hateoas;

import com.richiepanganiban.codingchallenge.domain.Client;
import com.richiepanganiban.codingchallenge.rest.controller.ClientNotFoundException;
import com.richiepanganiban.codingchallenge.rest.controller.ClientRestController;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class ClientResourceAssembler extends ResourceAssemblerSupport<Client, ClientResource>{

    public ClientResourceAssembler() {
        super(ClientRestController.class, ClientResource.class);
    }

    @Override
    public ClientResource toResource(Client client) {
        ClientResource resource = instantiateResource(client);
        resource.setName(client.getName());
        resource.setIpv4(client.getIpv4());
        resource.setEnvironment(client.getEnvironment());
        resource.setApp(client.getApp());

        try {
            resource.add(linkTo(methodOn(ClientRestController.class).get(client.getId())).withSelfRel());
            resource.add(linkTo(methodOn(ClientRestController.class).list()).withRel("list"));
            resource.add(linkTo(methodOn(ClientRestController.class).delete(client.getId())).withRel("delete"));
            resource.add(linkTo(methodOn(ClientRestController.class).filter("")).withRel("filter"));

        } catch (ClientNotFoundException e) {
            // Ignore, to be caught by controller
        }

        return resource;
    }
}