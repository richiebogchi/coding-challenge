package com.richiepanganiban.codingchallenge.rest.controller;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class IndexController {

    @GetMapping("/")
    public ResourceSupport index() {
        ServletUriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentRequestUri();
        ResourceSupport index = new ResourceSupport();
        index.add(ControllerLinkBuilder.linkTo(methodOn(ClientRestController.class).list()).withRel("clients"));
        index.add(new Link(builder.toUriString() + "api/docs").withRel("docs"));
        return index;
    }
}