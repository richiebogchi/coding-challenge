package com.richiepanganiban.codingchallenge.rest.controller;

import com.richiepanganiban.codingchallenge.domain.Client;
import com.richiepanganiban.codingchallenge.repository.ClientRepository;
import com.richiepanganiban.codingchallenge.rest.hateoas.ClientResource;
import com.richiepanganiban.codingchallenge.rest.hateoas.ClientResourceAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@ExposesResourceFor(ClientResource.class)
public class ClientRestController {

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    ClientResourceAssembler clientResourceAssembler;

    @GetMapping(value = "/clients", produces = MediaTypes.HAL_JSON_VALUE)
    public ResponseEntity<List<ClientResource>> list() {
        List<Client> clients = clientRepository.findAll();
        List<ClientResource> resourceList = clientResourceAssembler.toResources(clients);
        return new ResponseEntity<>(resourceList, HttpStatus.OK);
    }

    @GetMapping(value = "/clients/{id}", produces = MediaTypes.HAL_JSON_VALUE)
    public ResponseEntity<ClientResource> get(@PathVariable String id) throws ClientNotFoundException {
        Optional<Client> client = clientRepository.findById(id);
        if (client.isPresent()) {
            ClientResource resource = clientResourceAssembler.toResource(client.get());
            return new ResponseEntity<>(resource, HttpStatus.OK);
        } else {
            throw new ClientNotFoundException();
        }
    }

    @PostMapping(value = "/clients", consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE}, produces = MediaTypes.HAL_JSON_VALUE)
    public ResponseEntity<ClientResource> post(@RequestBody Client client) throws DuplicateClientException, ClientNotFoundException {

        Client existingClient = clientRepository.findByIpv4(client.getIpv4());
        if (existingClient != null) {
            throw new DuplicateClientException();
        }

        Client persistedClient = clientRepository.save(client);
        ClientResource resource = clientResourceAssembler.toResource(persistedClient);
        return new ResponseEntity<>(resource, HttpStatus.CREATED);
    }

    @GetMapping(value = "/clients/filter")
    public ResponseEntity<List<ClientResource>> filter(@RequestParam("query") String query) {
        List<Client> clients = clientRepository.filterBy(query);
        List<ClientResource> resourceList = clientResourceAssembler.toResources(clients);
        return new ResponseEntity<>(resourceList, HttpStatus.OK);
    }

    @DeleteMapping(value = "/clients/{id}", produces = MediaTypes.HAL_JSON_VALUE)
    public ResponseEntity<ClientResource> delete(@PathVariable String id) throws ClientNotFoundException {
        Optional<Client> client = clientRepository.findById(id);
        if (client.isPresent()) {
            clientRepository.deleteById(id);
            return buildCustomResponseContentByHttpStatus(HttpStatus.OK);
        } else {
            throw new ClientNotFoundException();
        }
    }

    @ExceptionHandler
    public ResponseEntity handleExceptions(Exception e) {
        HttpStatus httpStatus;

        if (e instanceof ClientNotFoundException) {
            httpStatus = HttpStatus.NOT_FOUND;
        } else if (e instanceof DuplicateClientException) {
            httpStatus = HttpStatus.BAD_REQUEST;
        } else {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        MultiValueMap<String, String> map = buildCustomResponseKeyPairMap(httpStatus.toString(), httpStatus.getReasonPhrase(), e.getMessage());
        return new ResponseEntity<>(map, httpStatus);
    }

    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity handleConstraintExceptions(Exception e) {
        HttpStatus badRequest = HttpStatus.BAD_REQUEST;
        MultiValueMap<String, String> map = buildCustomResponseKeyPairMap(badRequest.toString(), badRequest.getReasonPhrase(), e.getMessage());
        return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
    }

    private <T> ResponseEntity<T> buildCustomResponseContentByHttpStatus(HttpStatus httpStatus) {
        String message = "";
        if (HttpStatus.OK.equals(httpStatus)) {
            message = "Success";
        } else if (HttpStatus.NOT_FOUND.equals(httpStatus)) {
            message = "Resource not found";
        } else if (HttpStatus.INTERNAL_SERVER_ERROR.equals(httpStatus)) {
            message = "Uh oh.. Something went wrong.";
        }

        MultiValueMap<String, String> map = buildCustomResponseKeyPairMap(httpStatus.toString(), httpStatus.getReasonPhrase(), message);
        return new ResponseEntity<>(map, httpStatus);
    }

    private MultiValueMap<String, String> buildCustomResponseKeyPairMap(String code, String status, String message) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("code", code);
        map.add("status", status);
        map.add("message", message);
        return map;
    }

}
