package com.richiepanganiban.codingchallenge.rest.controller;

public class ClientNotFoundException extends Exception {

    public ClientNotFoundException() {
        super();
    }

    public ClientNotFoundException(String message) {
        super(message);
    }
}
