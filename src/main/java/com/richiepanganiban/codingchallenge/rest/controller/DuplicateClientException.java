package com.richiepanganiban.codingchallenge.rest.controller;

public class DuplicateClientException extends Exception {

    public DuplicateClientException() {
    }

    public DuplicateClientException(String message) {
        super(message);
    }

}
