package com.richiepanganiban.codingchallenge.repository;

import com.richiepanganiban.codingchallenge.domain.Client;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface ClientRepository extends MongoRepository<Client, String> {

    Client findByIpv4(String ipv4);

    @Query("{ '$or' : [ {'name' : {'$regex' : ?0} }, {'ipv4' : {'$regex' : ?0} }, {'app' : {'$regex' : ?0} }, {'environment' : {'$regex' : ?0} } ] }")
    List<Client> filterBy(String query);

}
