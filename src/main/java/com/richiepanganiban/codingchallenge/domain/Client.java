package com.richiepanganiban.codingchallenge.domain;

import com.richiepanganiban.codingchallenge.validator.Ipv4;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

@Document(collection = "client")
public class Client {

    @Id
    private String id;

    @Indexed(unique = true)
    @NotNull
    @Ipv4
    private String ipv4;

    @Indexed
    @NotNull
    private String name;

    @NotNull
    private String app;

    @NotNull
    private String environment;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIpv4() {
        return ipv4;
    }

    public void setIpv4(String ipv4) {
        this.ipv4 = ipv4;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }
}
