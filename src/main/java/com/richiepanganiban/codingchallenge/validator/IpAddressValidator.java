package com.richiepanganiban.codingchallenge.validator;

import org.apache.commons.validator.routines.InetAddressValidator;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IpAddressValidator implements ConstraintValidator<Ipv4, String> {

    @Autowired
    InetAddressValidator inetAddressValidator;


    @Override
    public void initialize(Ipv4 constraintAnnotation) {

    }

    @Override
    public boolean isValid(String ipv4, ConstraintValidatorContext constraintValidatorContext) {
        return inetAddressValidator.isValid(ipv4);
    }
}
